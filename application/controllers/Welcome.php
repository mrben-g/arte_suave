<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->data['vista'] = 'paginas/index';
		$this->load->view('layouts/inicio');
	}

	public function about()
	{
	   $this->data['vista'] = 'paginas/about';
		 $this->load->view('layouts/inicio');
	}

	public function galeria()
	{
	   $this->data['vista'] = 'paginas/galeria';
		 $this->load->view('layouts/inicio');
	}

	public function programs()
	{
	   $this->data['vista'] = 'paginas/programs';
		 $this->load->view('layouts/inicio');
	}

	public function calendar()
	{
		$this->data['vista'] = 'paginas/calendario';
	 $this->load->view('layouts/inicio');
	}

	public function contact()
	{
		$this->data['vista'] = 'paginas/contact';
	 $this->load->view('layouts/inicio');
	}
}
