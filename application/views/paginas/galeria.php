    <!-- Divider -->
            <hr class="mt-0 mb-0 "/>
            <!-- End Divider --> 

<!-- Portfolio Section -->
            <section class="page-section pt-0 pb-90">
                <div class="container relative">
                    <!-- Works Filter -->                    
                    <div class="works-filter align-left">
                        <a href="#" class="filter active" data-filter="*">All works</a>
                        <a href="#combat" class="filter" data-filter=".combat">Combat</a>
                        <a href="#kids" class="filter" data-filter=".kids">Kids</a>
                        <!--<a href="#profesors" class="filter" data-filter=".profesors">Profesors</a>-->
                        <a href="#team" class="filter" data-filter=".team">Team</a>
                        <a href="#self-defense" class="filter" data-filter=".self-defense">Self Defense</a>
                        <a href="#videos" class="filter" data-filter=".videos">Videos</a>
                    </div>                    
                    <!-- End Works Filter -->
                    
                    <!-- Works Grid -->
                    <ul class="works-grid work-grid-3 work-grid-gut clearfix font-alt hide-titles" id="work-grid">
                        <!-- Work Item (Lightbox) -->
                        <li class="work-item mix combat">
                            <a href="<?php echo base_url('public/grandes/') ?>combat_.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img src="<?php echo base_url('public/pequenas') ?>/combat_1_p.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                        
                        
                          <!-- Work Item (Lightbox) -->
                        <li class="work-item mix combat">
                            <a href="<?php echo base_url('public/grandes/combat/') ?>1.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img src="<?php echo base_url('public/pequenas/combat/') ?>1.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                          <!-- Work Item (Lightbox) -->
                        <li class="work-item mix combat">
                            <a href="<?php echo base_url('public/grandes/combat/') ?>2.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img src="<?php echo base_url('public/pequenas/combat/') ?>2.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                          <!-- Work Item (Lightbox) -->
                        <li class="work-item mix combat">
                            <a href="<?php echo base_url('public/grandes/combat/') ?>4.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img src="<?php echo base_url('public/pequenas/combat/') ?>4.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                          <!-- Work Item (Lightbox) -->
                        <li class="work-item mix combat">
                            <a href="<?php echo base_url('public/grandes/combat/') ?>5.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img src="<?php echo base_url('public/pequenas/combat/') ?>5.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                          <!-- Work Item (Lightbox) -->
                        <li class="work-item mix combat">
                            <a href="<?php echo base_url('public/grandes/combat/') ?>6.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img src="<?php echo base_url('public/pequenas/combat/') ?>6.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                          <!-- Work Item (Lightbox) -->
                        <li class="work-item mix combat">
                            <a href="<?php echo base_url('public/grandes/combat/') ?>9.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img src="<?php echo base_url('public/pequenas/combat/') ?>9.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                          <!-- Work Item (Lightbox) -->
                        <li class="work-item mix combat">
                            <a href="<?php echo base_url('public/grandes/combat/') ?>13.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img src="<?php echo base_url('public/pequenas/combat/') ?>13.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                          <!-- Work Item (Lightbox) -->
                        <li class="work-item mix combat">
                            <a href="<?php echo base_url('public/grandes/combat/') ?>14.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img src="<?php echo base_url('public/pequenas/combat/') ?>14.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                          <!-- Work Item (Lightbox) -->
                        <li class="work-item mix combat">
                            <a href="<?php echo base_url('public/grandes/combat/') ?>17.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img src="<?php echo base_url('public/pequenas/combat/') ?>17.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                          <!-- Work Item (Lightbox) -->
                        <li class="work-item mix combat">
                            <a href="<?php echo base_url('public/grandes/combat/') ?>18.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img src="<?php echo base_url('public/pequenas/combat/') ?>18.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                          <!-- Work Item (Lightbox) -->
                        <li class="work-item mix combat">
                            <a href="<?php echo base_url('public/grandes/combat/') ?>22.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img src="<?php echo base_url('public/pequenas/combat/') ?>22.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                          <!-- Work Item (Lightbox) -->
                        <li class="work-item mix combat">
                            <a href="<?php echo base_url('public/grandes/combat/') ?>23.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img src="<?php echo base_url('public/pequenas/combat/') ?>23.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                        
                        
                        
                        <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>1.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>1.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                       
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                        
                        <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>2.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>2.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                     
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                        
                        
                               
                        <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>3.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>3.jpg" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                       
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                               
                          <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>4.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>4.jpg" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                       
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                      <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>5.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>5.jpg" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                       
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                            <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>6.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>6.jpg" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                       
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                            <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>7.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>7.jpg" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                       
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                            <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>8.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>8.jpg" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                       
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                            <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>9.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>9.jpg" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                       
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                            <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>10.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>10.jpg" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                       
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                            <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>11.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>11.jpg" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                       
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                            <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>12.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>12.jpg" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                       
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                            <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>13.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>13.jpg" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                       
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                                 <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>14.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>14.jpg" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                       
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                                 <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>15.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>15.jpg" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                       
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                                 <!-- Work Item (External Page) -->
                        <li class="work-item mix kids">
                            <a href="<?php echo base_url('public/grandes/kids/') ?>16.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/kids/') ?>16.jpg" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                       
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                        
                        
                        
                        <!-- Work Item (External Page) --
                        <li class="work-item mix self-defense">
                            <a href="<?php echo base_url('public/grandes/') ?>selfdefense_1.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/') ?>selfdefense_1_p.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                        
                        <!-- Work Item (External Page) --
                        <li class="work-item mix self-defense">
                            <a href="<?php echo base_url('public/grandes/') ?>selfdefense_2.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/') ?>selfdefense_2_p.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                        
                        <!-- Work Item (External Page) -->
                        <li class="work-item mix team">
                            <a href="<?php echo base_url('public/grandes/team/') ?>1.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/team/') ?>1_thumb.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                    
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                        
                       <!-- Work Item (External Page) -->
                        <li class="work-item mix team">
                            <a href="<?php echo base_url('public/grandes/team/') ?>2.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/team/') ?>2_thumb.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                    
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                           <!-- Work Item (External Page) -->
                        <li class="work-item mix team">
                            <a href="<?php echo base_url('public/grandes/team/') ?>3.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/team/') ?>3_thumb.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                    
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                           <!-- Work Item (External Page) -->
                        <li class="work-item mix team">
                            <a href="<?php echo base_url('public/grandes/team/') ?>4.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/team/') ?>4_thumb.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                    
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                                <!-- Work Item (External Page) -->
                        <li class="work-item mix team">
                            <a href="<?php echo base_url('public/grandes/team/') ?>5.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/team/') ?>5_thumb.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                    
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                                <!-- Work Item (External Page) -->
                        <li class="work-item mix team">
                            <a href="<?php echo base_url('public/grandes/team/') ?>6.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/team/') ?>6_thumb.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                    
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                                <!-- Work Item (External Page) -->
                        <li class="work-item mix team">
                            <a href="<?php echo base_url('public/grandes/team/') ?>7.jpg" class="lightbox-gallery-2 mfp-image">
                                <div class="work-img">
                                    <img class="work-img" src="<?php echo base_url('public/pequenas/team/') ?>7_thumb.jpg" alt="Work" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                    
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End Work Item -->
                        
                        
                        
                        
                       <!-- video 1 (Lightbox) -->
                        <li class="work-item mix videos">
                            <a href="<?php echo base_url('public/videos/') ?>IMG_2399.MP4" class="lightbox-gallery-1 mfp-iframe">
                                <div class="work-img">
                                    <img src="<?php echo base_url('public/videos') ?>/thumb/v-3999.png" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End video -->
                         <!-- video 2 (Lightbox) -->
                        <li class="work-item mix videos">
                            <a href="<?php echo base_url('public/videos/') ?>IMG_2400.MP4" class="lightbox-gallery-1 mfp-iframe">
                                <div class="work-img">
                                   <img src="<?php echo base_url('public/videos') ?>/thumb/v-2400.png" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End video -->
                         <!-- video 3 (Lightbox) -->
                        <li class="work-item mix videos">
                            <a href="<?php echo base_url('public/videos/') ?>IMG_2401.MP4" class="lightbox-gallery-1 mfp-iframe">
                                <div class="work-img">
                                    <img src="<?php echo base_url('public/videos') ?>/thumb/v-2401.png" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End video -->
                         <!-- video 4 (Lightbox) -->
                        <li class="work-item mix videos">
                            <a href="<?php echo base_url('public/videos/') ?>IMG_2402.MP4" class="lightbox-gallery-1 mfp-iframe">
                                <div class="work-img">
                                  <img src="<?php echo base_url('public/videos') ?>/thumb/v-2402.png" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End video -->
                         <!-- video 5(Lightbox) -->
                        <li class="work-item mix videos">
                            <a href="<?php echo base_url('public/videos/') ?>IMG_2403.MP4" class="lightbox-gallery-1 mfp-iframe">
                                <div class="work-img">
                                   <img src="<?php echo base_url('public/videos') ?>/thumb/v-2403.png" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End video -->
                         <!-- video 6 (Lightbox) -->
                        <li class="work-item mix videos">
                            <a href="<?php echo base_url('public/videos/') ?>IMG_2404.MP4" class="lightbox-gallery-1 mfp-iframe">
                                <div class="work-img">
                                  <img src="<?php echo base_url('public/videos') ?>/thumb/v-2404.png" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End video -->
                         <!-- video 7 (Lightbox) -->
                        <li class="work-item mix videos">
                            <a href="<?php echo base_url('public/videos/') ?>IMG_2405.MP4" class="lightbox-gallery-1 mfp-iframe">
                                <div class="work-img">
                                   <img src="<?php echo base_url('public/videos') ?>/thumb/v-2405.png" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End video -->
                         <!-- video 8 (Lightbox) -->
                        <li class="work-item mix videos">
                            <a href="<?php echo base_url('public/videos/') ?>IMG_2406.MP4" class="lightbox-gallery-1 mfp-iframe">
                                <div class="work-img">
                             <img src="<?php echo base_url('public/videos') ?>/thumb/v-2406.png" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End video -->
                         <!-- video 9 (Lightbox) -->
                        <li class="work-item mix videos">
                            <a href="<?php echo base_url('public/videos/') ?>IMG_2407.MP4" class="lightbox-gallery-1 mfp-iframe">
                                <div class="work-img">
                                  <img src="<?php echo base_url('public/videos') ?>/thumb/v-2407.png" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End video -->
                         <!-- video 10 (Lightbox) -->
                        <li class="work-item mix videos">
                            <a href="<?php echo base_url('public/videos/') ?>IMG_2408.MP4" class="lightbox-gallery-1 mfp-iframe">
                                <div class="work-img">
                                <img src="<?php echo base_url('public/videos') ?>/thumb/v-2408.png" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End video -->
                         <!-- video 11 (Lightbox) -->
                        <li class="work-item mix videos">
                            <a href="<?php echo base_url('public/videos/') ?>IMG_2409.MP4" class="lightbox-gallery-1 mfp-iframe">
                                <div class="work-img">
                              <img src="<?php echo base_url('public/videos') ?>/thumb/v-2409.png" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End video -->
                         <!-- video 12 (Lightbox) -->
                        <li class="work-item mix videos">
                            <a href="<?php echo base_url('public/videos/') ?>IMG_2410.MP4" class="lightbox-gallery-1 mfp-iframe">
                                <div class="work-img">
                                 <img src="<?php echo base_url('public/videos') ?>/thumb/v-2410.png" alt="" />
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"></h3>
                                    <div class="work-descr">
                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- End video -->
                        
                    </ul>
                    <!-- End Works Grid -->
                    
                </div>
            </section>
            <!-- End Portfolio Section -->


<!--<section>
        <div class="container">


            <div class="row">
                <div class="col-sm-12">
                    <div class="lightbox-grid square-thumbs" data-gallery-title="Gallery">
                        <ul>
                            <li>
                                <a href="<?php echo base_url('public/theme') ?>/img/img_about.png" data-lightbox="true" data-title="image">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="<?php echo base_url('public/theme') ?>/img/img_about.png">
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('public/theme') ?>/img/galeria2.png" data-lightbox="true" data-title="image">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="<?php echo base_url('public/theme') ?>/img/galeria2.png">
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('public/theme') ?>/img/galeria3.png" data-lightbox="true" data-title="image">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="<?php echo base_url('public/theme') ?>/img/galeria3.png">
                                    </div>
                                </a>
                            </li><li>
                                <a href="<?php echo base_url('public/theme') ?>/img/galeria4.png" data-lightbox="true" data-title="image">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="<?php echo base_url('public/theme') ?>/img/galeria4.png">
                                    </div>
                                </a>
                            </li><li>
                                <a href="<?php echo base_url('public/theme') ?>/img/galeria5.png" data-lightbox="true" data-title="image">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="<?php echo base_url('public/theme') ?>/img/galeria5.png">
                                    </div>
                                </a>
                            </li><li>
                                <a href="<?php echo base_url('public/theme') ?>/img/galeria6.png" data-lightbox="true" data-title="image">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="<?php echo base_url('public/theme') ?>/img/galeria6.png">
                                    </div>
                                </a>
                            </li>





                        </ul>
                    </div>

                </div>
            </div>

        </div>

    </section>
-->