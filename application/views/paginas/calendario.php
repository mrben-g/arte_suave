<style media="screen">
.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
  padding: 16px;
}
.link_red{
  color: black;
}
a:hover {
    color: #ff0000;
}
</style>
<!-- Divider -->
<hr class="mt-0 mb-0 "/>
<!-- End Divider --> 
<section class="page-section pt-30">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-left">
                <h1 class="">SCHEDULE</h1>
                <hr style="top: -19px;position: relative;border-top: 5px solid #ff0000;width:50%;">
            </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="pull-right">
              <a class="link_red" href="#">Brazilian Jiu Jitsu /</a>
              <a class="link_red" href="#">MMA /</a>
              <a class="link_red" href="#">Muay Thai /</a>
              <a class="link_red" href="#">Selfdefense /</a>
              <a class="link_red" href="#">Kids /</a>
              <a class="link_red" href="#">Fitness </a>
            </div>

          </div>
          <div class="col-sm-12">
            <div class='table-responsive'>
              <table class='table table-striped table-bordered table-hover table-condensed'>
                <thead>
                  <tr>
                    <th></th>
                    <th>Monday</th>
                    <th>Tuesday</th>
                    <th>Wednesday</th>
                    <th>Thursday</th>
                    <th>Friday</th>
<!--                    <th>Saturday</th>
                    <th>Sunday</th>-->
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $horas = array(
                      '8-9 AM',
                      '9-10 AM',
                      '4-5 PM',
                      '5-6 PM',
                      '6-7 PM',
                      '7-8 PM',
                      '8-9 PM',
                    );
                   ?>
                   <?php foreach ($horas as $key => $row): ?>
                     <tr>
                       <td><?php echo $row; ?></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
<!--                       <td></td>
                       <td></td>-->
                     </tr>
                   <?php endforeach; ?>

                </tbody>
              </table>
            </div>
          </div>

        </div>
</div>
    </section>
