<!-- Divider -->
<hr class="mt-0 mb-0 "/>
<!-- End Divider --> 
<!--<section class="page-section pt-30 pb-30">
    <iframe src="http://www.mrwasabiserver.com/webpage/artesuave/contact_frame.php" scrolling="no" width="100%" height="516px"></iframe>
      </section>-->
                     <!-- Home Section -->
            <section class="page-section pb-100">
               

                            <div class="container align-center">
                                 <div class="container relative">

                    <h2 style="" class="section-title font-alt mb-40 mb-sm-10">
                       <img src="<?php echo base_url('public') ?>/freeclass.png" alt="" />
                    </h2>

                    <div class="row mb-60 mb-xs-10">

                        <div class="col-md-10 col-md-offset-1">
                            <div class="row">

                                <!-- Phone -->
                                <div class="col-sm-6 col-lg-4 pt-20 pb-10 pb-xs-0">
                                    <div class="contact-item">
                                        <div class="ci-icon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="ci-title font-alt">
                                           Phone
                                        </div>
                                        <div class="ci-text">
                                           (956) 342 9467
                                        </div>
                                    </div>
                                </div>
                                <!-- End Phone -->

                                <!-- Address -->
                                <div class="col-sm-6 col-lg-4 pt-20 pb-10 pb-xs-0">
                                    <div class="contact-item">
                                        <div class="ci-icon">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="ci-title font-alt">
                                            Address
                                        </div>
                                        <div class="ci-text">
                                           228 N.Ware Rd. Suite 30 McAllen Tx. 78501
                                        </div>
                                    </div>
                                </div>
                                <!-- End Address -->

                                <!-- Email -->
                                <div class="col-sm-6 col-lg-4 pt-20 pb-10 pb-xs-0">
                                    <div class="contact-item">
                                        <div class="ci-icon">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                        <div class="ci-title font-alt">
                                            Email
                                        </div>
                                        <div class="ci-text">
                                            <a href="mailto:">artesuavebjjmission@gmail.com</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Email -->

                            </div>
                        </div>

                    </div>

                    <!-- Contact Form -->
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 ">

                            <form class="form contact-form" id="contact_form">
                                <div class="clearfix">

                                    <div class="cf-left-col">
                                        <!-- Name -->
                                        <div class="form-group">
                                            <input type="text" name="name" id="name" class="input-md round form-control mb-0" placeholder="Name" pattern=".{3,100}" required>
                                        </div>
                                        <!-- Email -->
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" class="input-md round form-control mb-0" placeholder="E-mail" pattern=".{5,100}" required>
                                        </div>
                                    </div>

                                    <div class="cf-right-col">
                                        <!-- Message -->
                                        <div class="form-group">
                                            <textarea name="message" id="message" class="input-md round form-control" style="height: 84px;" placeholder="Message"></textarea>
                                        </div>
                                    </div>

                                </div>

                                <div class="clearfix">
                                    <div class="cf-left-col">
                                        <!-- Inform Tip -->
                                        <div class="form-tip pt-xs-10 text-left hidden-xs">
                                            <i class="fa fa-info-circle"></i> All the fields are required
                                        </div>
                                    </div>
                                    <div class="cf-right-col">
                                        <!-- Send Button -->
                                        <div class="align-right pt-10 pb-xs-20 mb-xs-50 pb-20">
                                            <button class="submit_btn btn btn-mod btn-medium btn-round " id="submit_btn" style="">Send Message</button>
                                        </div>
                                    </div>
                                </div>
                               <div id="result"></div>
                            </form>

                        </div>
                    </div>
                    <!-- End Contact Form -->

                </div>

                            </div>

                   
                    <!-- End Hero Content -->

                    <!-- Scroll Down --
                    <div class="local-scroll">
                        <a href="#about" class="scroll-down"><i class="fa fa-angle-down scroll-down-icon"></i></a>
                    </div>
                    <!-- End Scroll Down -->

               
            </section>
            <!-- End Home Section -->
