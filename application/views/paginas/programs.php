<!-- Divider -->
<hr class="mt-0 mb-0 "/>
<!-- End Divider --> 
<section class="page-section pt-30">
    <div class="container">
        <div class="row">
  <div class="col-md-6 text-center" >
    <h1 class="text-center" >OUR PROGRAMS</h1>
    <hr style=" top: -19px; position: relative;   border-top: 5px solid #ff0000;width: 24%;margin-left: 37%;">
  </div>
</div>
        <div class="row pb-20">
            <div class="col-md-6 text-justify">
                <h3 class="mb-0">Brazilian Jiu Jitsu</h3>
                    Brazilian Jiu-Jitsu or BJJ (also written as jujitsu or jujutsu) is a martial art of Japanese origin in which one essentially uses levers, torsions and pressure in order to take one’s opponent to the ground and dominate them. Literally, jū in Japanese means ‘gentleness,’ and jutsu means ‘art,’ ‘technique.’ Hence the literal translation by which it’s also known, the ‘gentle art.<br><br>
This martial art was led by one of Kano's emissaries, Mitsuyo Maeda (aka Conde Koma) who traveled and taught hand-to-hand combat around the world in the early 20th century. While in South America, his teachings were passed onto the Gracie family, some feisty and entrepreneurial folks who saw plenty of potential in the Japanese export.
              
              
            </div>
            <div class="col-md-6 text-center">
                <img alt="image"  src="<?php echo base_url('public/01.png') ?>" class="" style="width: 100%;">
            </div>
        </div>
        <div class="row pb-20">
            <div class="col-md-6 text-center">
                  <img alt="image" src="<?php echo base_url('public/02.png') ?>" style="width: 100%;">
            </div>
            <div class="col-md-6 text-justify">
                <h3 class="mb-0">Mixes martial arts (MMA)</h3>
                Full-contact combat sport that allows both striking and grappling, both standing and on the ground, using techniques from other combat sports and martial arts. The first documented use of the term mixed martial arts was in a review of UFC 1 by television critic Howard Rosenberg in 1993<br>
                Sport in which two competitors attempt to achieve dominance over one another by utilizing three general tactics: striking, finishing holds, and control. The rules allow the combatants to use a variety of martial arts techniques including punches, kicks, joint-locks, chokes, takedowns and throws. Victory is normally gained through knock-out, submission or stoppage by the referee, the fight doctor, or a competitor's cornerman.
            </div>
        </div>
        <div class="row pb-20">
            <div class="col-md-6 text-justify">
           <h3 class="mb-0">Muay Thai</h3>
           Muay Thai is a form of hard martial art practiced in large parts of the world, including Thailand and other Southeast Asian countries. The art is similar to others in Southeast Asia such as: pradal serey in Cambodia, lethwei in Myanmar, tomoi in Malaysia, and Lao boxing in Laos. Muay Thai has a long history in Thailand and is the country’s national sport. Traditional Muay Thai practiced today varies significantly from the ancient art muay boran and uses kicks and punches in a ring with gloves similar to those used in Western boxing.<br>
           Muay Thai is referred to as “The Art of the Eight Limbs“, as the hands, shins, elbows, and knees are all used extensively in this art.
            </div>
            <div class="col-md-6 text-center">
                  <img alt="image" src="<?php echo base_url('public/03.png') ?>" style="width: 100%;">
            </div>
        </div>
        <div class="row pb-20">
            <div class="col-md-6 text-center">
                  <img alt="image" class="background-image" src="<?php echo base_url('public/04.png') ?>" style="width: 100%;">
            </div>
            <div class="col-md-6 text-justify">
                <h3 class="mb-0">Self defence</h3>
                Is a countermeasure that involves defending the health and well-being of oneself from harm. The use of the right of self-defense as a legal justification for the use of force in times of danger is available in many jurisdictions, but the interpretation varies widely.<br>
Self-defense education is part of the martial arts industry in the wider sense. self-defense courses are marketed explicitly as being oriented towards effectiveness and optimized towards situations as they occur in the real world. There are a large number of systems taught commercially, many tailored to the needs of specific target audiences (e.g. defense against attempted rape for women, self-defense for children and teens).
            </div>
        </div>
        <div class="row pb-20">
            <div class="col-md-6 text-justify">
                <h3 class="mb-0">Kid Clases</h3>
            
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br>
            </p>
            </div>
            <div class="col-md-6 text-center">
                  <img alt="image" class="background-image" src="<?php echo base_url('public/05.png') ?>" style="width: 100%;">
            </div>
        </div>
        <div class="row pb-20">
            <div class="col-md-6 text-center">
                  <img alt="image" class="background-image" src="<?php echo base_url('public/06.png') ?>" style="width: 100%;">
            </div>
            <div class="col-md-6 text-justify">
               <h3 class="mb-0" >Fitness Program</h3>
            
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br>
            
            </div>
        </div>
    </div>
    
</section>



<!--<style media="screen">

@media (min-width: 1281px) {

  .background-image-holder {
    position: absolute;
    width: 94%;
    height: 79%;
    top: 0;
    left: 0;
    z-index: 1;
    background: #292929;
    background-size: cover !important;
    background-position: 50% 50% !important;
    transition: all 0.3s ease;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    opacity: 0;
  }
  .image-square {
      height: 50vw;
      max-height: 633px;
      overflow: hidden;
      padding: 0;
  }
  .background-image-holder {
      position: absolute;
      width: 88%;
          height: 76%;
      top: 116px;

      z-index: 1;
      background: #292929;
      background-size: cover !important;
      background-position: 50% 50% !important;
      transition: all 0.3s ease;
      -webkit-transition: all 0.3s ease;
      -moz-transition: all 0.3s ease;
      opacity: 0;
  }
  .right-image{
    left: -9px;
  }
  .left-image{
    left: 77px;
  }
  .image-square .col-md-6 {
    top: -105px;
  }
  .image-square > .content {
    text-align: justify;
      padding: 0% 6%;
      top: 33%;
      transform: translate3d(0, -50%, 0);
      -webkit-transform: translate3d(0, -50%, 0);
      -moz-transform: translate3d(0, -50%, 0);
  }
  .image-square {
      height: 34vw;
      max-height: 615px;
      overflow: hidden;
      padding: 0;
  }


}

</style>
<div class="row">
  <div class="col-md-6 text-center" >
    <h1 class="text-center">OUR PROGRAMS</h1>
    <hr style=" top: -19px;    position: relative;   border-top: 7px solid red;    width: 24%;margin-left: 37%;">
  </div>
</div>
<section class="image-square right">

        <div class="image col-md-6">
            <div class="background-image-holder right-image">
                <img alt="image" class="background-image" src="<?php echo base_url('public/01.png') ?>">
            </div>
        </div>
        <div class="col-md-6 content">
            <h3>Brazilian Jiu Jitsu</h3>
            <p class="mb0">
              Brazilian Jiu-Jitsu or BJJ (also written as jujitsu or jujutsu) is a martial art of Japanese origin in which one essentially uses levers, torsions and pressure in order to take one’s opponent to the ground and dominate them. Literally, jū in Japanese means ‘gentleness,’ and jutsu means ‘art,’ ‘technique.’ Hence the literal translation by which it’s also known, the ‘gentle art.

This martial art was led by one of Kano's emissaries, Mitsuyo Maeda (aka Conde Koma) who traveled and taught hand-to-hand combat around the world in the early 20th century. While in South America, his teachings were passed onto the Gracie family, some feisty and entrepreneurial folks who saw plenty of potential in the Japanese export.
              <br></p>
        </div>
    </section><section class="image-square left">
        <div class="col-md-6 image">
            <div class="background-image-holder left-image">
                <img alt="image" class="background-image" src="<?php echo base_url('public/02.png') ?>">
            </div>
        </div>
        <div class="col-md-6 col-md-offset-1 content">
            <h3>Mixes martial arts (MMA)</h3>
            <p class="mb0">
              Full-contact combat sport that allows both striking and grappling, both standing and on the ground, using techniques from other combat sports and martial arts. The first documented use of the term mixed martial arts was in a review of UFC 1 by television critic Howard Rosenberg in 1993

Sport in which two competitors attempt to achieve dominance over one another by utilizing three general tactics: striking, finishing holds, and control. The rules allow the combatants to use a variety of martial arts techniques including punches, kicks, joint-locks, chokes, takedowns and throws. Victory is normally gained through knock-out, submission or stoppage by the referee, the fight doctor, or a competitor's cornerman.

              <br></p>
        </div>
    </section><section class="image-square right">
        <div class="col-md-6 image">
            <div class="background-image-holder right-image">
                <img alt="image" class="background-image" src="<?php echo base_url('public/03.png') ?>">
            </div>
        </div>
        <div class="col-md-6 content">
            <h3>Muay Thai</h3>
            <p class="mb0">
              Muay Thai is a form of hard martial art practiced in large parts of the world, including Thailand and other Southeast Asian countries. The art is similar to others in Southeast Asia such as: pradal serey in Cambodia, lethwei in Myanmar, tomoi in Malaysia, and Lao boxing in Laos. Muay Thai has a long history in Thailand and is the country’s national sport. Traditional Muay Thai practiced today varies significantly from the ancient art muay boran and uses kicks and punches in a ring with gloves similar to those used in Western boxing.

Muay Thai is referred to as “The Art of the Eight Limbs“, as the hands, shins, elbows, and knees are all used extensively in this art.

              <br>
            </p>
        </div>
    </section><section class="image-square left">
        <div class="col-md-6 image">
            <div class="background-image-holder left-image">
                <img alt="image" class="background-image" src="<?php echo base_url('public/04.png') ?>">
            </div>
        </div>
        <div class="col-md-6 col-md-offset-1 content">
            <h3>Self defence</h3>
            <p class="mb0">
              Is a countermeasure that involves defending the health and well-being of oneself from harm. The use of the right of self-defense as a legal justification for the use of force in times of danger is available in many jurisdictions, but the interpretation varies widely.

              Self-defense education is part of the martial arts industry in the wider sense. self-defense courses are marketed explicitly as being oriented towards effectiveness and optimized towards situations as they occur in the real world. There are a large number of systems taught commercially, many tailored to the needs of specific target audiences (e.g. defense against attempted rape for women, self-defense for children and teens).
<br></p>
        </div>
    </section><section class="image-square right">
        <div class="col-md-6 image">
            <div class="background-image-holder right-image">
                <img alt="image" class="background-image" src="<?php echo base_url('public/05.png') ?>">
            </div>
        </div>
        <div class="col-md-6 content">
            <h3>Kid Clases</h3>
            <p class="mb0">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br>
            </p>
        </div>
    </section><section class="image-square left">
        <div class="col-md-6 image">
            <div class="background-image-holder left-image">
                <img alt="image" class="background-image" src="<?php echo base_url('public/06.png') ?>">
            </div>
        </div>
        <div class="col-md-6 col-md-offset-1 content">
            <h3>Fitness Program</h3>
            <p class="mb0">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br>
            </p>
        </div>
    </section>
-->