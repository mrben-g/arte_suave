<!--
  <section style="max-height: 438px;" class="image-slider slider-all-controls controls-inside pt0 pb0 height-70">
    <ul class="slides">
      <li class="overlay image-bg">
        <div class="background-image-holder">
          <img alt="image" class="background-image" src="<?php echo base_url('public/theme') ?>/img/home_kids.png">
        </div>
        <div class="container v-align-transform">
          <div class="row text-center">
            <div class="col-md-10 col-md-offset-1">
              <h2 class="mb-xs-16 bold">BRAZILIAN JIU JITSU<br>FOR KIDS</h2>



            </div>
          </div>

        </div>

      </li>
      <li class="overlay image-bg">
        <div class="background-image-holder">
          <img alt="image" class="background-image" src="<?php echo base_url('public/theme') ?>/img/home_kids.png">
        </div>
        <div class="container v-align-transform">
          <div class="row text-center">
            <div class="col-md-10 col-md-offset-1">
              <h2 class="mb-xs-16 bold">BRAZILIAN JIU JITSU<br>FOR KIDS</h2>



            </div>
          </div>

        </div>

      </li>
    </ul>
  </section>-->
   <!-- Fullwidth Slider -->
            <div class="home-section fullwidth-slider" id="home">
                
                <!-- Slide Item -->
                <section class="home-section bg-scroll fixed-height-small " data-background="<?php echo base_url('public/theme') ?>/img/home_kids.png">
                    <div class="js-height-parent container">
                        
                        <!-- Hero Content -->
                        <div class="home-content">
                            <div class="home-text">
                                
                                <div class="hs-line-11 no-transp mb-40 mb-xs-10 white">
                                  BRAZILIAN JIU JITSU<br>FOR KIDS
                                </div>
                                
                                <div class="hs-line-14 font-alt mb-50 mb-xs-10">
                                  
                                </div>
                                
                                <div class="local-scroll">
<!--                                    <a href="pages-pricing-1.html" class="btn btn-mod btn-border-w btn-circle btn-small">Get Pricing</a>
                                    <span class="hidden-xs">&nbsp;</span>
                                    <a href="#about" class="btn btn-mod btn-border-w btn-circle btn-small">Learn More</a>-->
                                </div>
                                
                            </div>
                        </div>
                        <!-- End Hero Content -->

                    </div>
                </section>
                <!-- End Slide Item -->
                   <!-- Slide Item -->
                <section class="home-section bg-scroll fixed-height-small " data-background="<?php echo base_url('public/theme') ?>/img/home_kids1.png">
                    <div class="js-height-parent container">
                        
                        <!-- Hero Content -->
                        <div class="home-content">
                            <div class="home-text">
                                
                                <div class="hs-line-11 no-transp mb-40 mb-xs-10 white">
                                  BRAZILIAN JIU JITSU<br>FOR KIDS
                                </div>
                                
                                <div class="hs-line-14 font-alt mb-50 mb-xs-10">
                                  
                                </div>
                                
                                <div class="local-scroll">
<!--                                    <a href="pages-pricing-1.html" class="btn btn-mod btn-border-w btn-circle btn-small">Get Pricing</a>
                                    <span class="hidden-xs">&nbsp;</span>
                                    <a href="#about" class="btn btn-mod btn-border-w btn-circle btn-small">Learn More</a>-->
                                </div>
                                
                            </div>
                        </div>
                        <!-- End Hero Content -->

                    </div>
                </section>
                <!-- End Slide Item -->
                <!-- Slide Item -->
                <section class="home-section bg-scroll fixed-height-small " data-background="<?php echo base_url('public/theme') ?>/img/home_kids2.jpg">
                    <div class="js-height-parent container">
                        
                        <!-- Hero Content -->
                        <div class="home-content">
                            <div class="home-text">
                                
                               <div class="hs-line-11 no-transp mb-40 mb-xs-10 white">
                                  BRAZILIAN JIU JITSU<br>FOR KIDS
                                </div>
                                
                                <h2 class="hs-line-14 font-alt mb-50 mb-xs-10">
                                  
                                </h2>
                                
                                <div class="local-scroll">
<!--                                    <a href="#about" class="btn btn-mod btn-border-w btn-small btn-circle hidden-xs">See More</a>
                                    <span class="hidden-xs">&nbsp;</span>
                                    <a href="http://vimeo.com/50201327" class="btn btn-mod btn-border-w btn-small btn-circle lightbox mfp-iframe">Play Reel</a>-->
                                </div>
                                
                            </div>
                        </div>
                        <!-- End Hero Content -->
                        
                    </div>
                </section>
                <!-- End Slide Item -->
                
                <!-- Slide Item -->
                <section class="home-section bg-scroll fixed-height-small " data-background="<?php echo base_url('public/theme') ?>/img/home_kids3.png">
                    <div class="js-height-parent container">
                        
                        <!-- Hero Content -->
                        <div class="home-content">
                            <div class="home-text">
                                
                               <div class="hs-line-11 no-transp mb-40 mb-xs-10 white">
                                  BRAZILIAN JIU JITSU<br>FOR KIDS
                                </div>                 
                                
                                <h1 class="hs-line-14 font-alt mb-50 mb-xs-10">
                              
                                </h1>
                                
                                <div>
<!--                                    <a href="pages-pricing-1.html" class="btn btn-mod btn-border-w btn-small btn-circle">Get Pricing</a>-->
                                </div>
                                
                            </div>
                        </div>
                        <!-- End Hero Content -->
                        
                    </div>
                </section>
                <!-- End Slide Item -->
            
            </div>
            <!-- End Fullwidth Slider -->
            <div class="pb-xs-100"></div>








  <!-- <footer class="footer-2 bg-dark text-center-xs">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <a href="#"><img class="image-xxs fade-half" alt="Pic" src="img/logo-light.png"></a>
        </div>

        <div class="col-sm-4 text-center">
          <span class="fade-half">
              © Copyright 2015 Medium Rare - All Rights Reserved
            </span>
        </div>

        <div class="col-sm-4 text-right text-center-xs">
          <ul class="list-inline social-list">
            <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
            <li><a href="#"><i class="ti-facebook"></i></a></li>
            <li><a href="#"><i class="ti-dribbble"></i></a></li>
            <li><a href="#"><i class="ti-vimeo-alt"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer> -->
