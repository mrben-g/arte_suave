     <!-- Divider -->
            <hr class="mt-0 mb-0 "/>
            <!-- End Divider -->
  
            <!-- About Section -->
            <section class="page-section pt-30 pb-90" id="about">
                <div class="container relative">
                    <div class="section-text">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 mb-sm-50 mb-xs-30 ">
                            <h1 class="text-center">ARTE SUAVE ACADEMY</h1>
                  <hr style=" top: -16px;position: relative;border-top: 5px solid #ff0000;width: 24%;margin-left: 33%;">
                  <div class="row">
                      <div class="col-md-12 mt-80 mb-80">
                  <h5 class="text-center" style="font-weight: bold;">Instructor Edmilson Freitas "Junior"</h5>
                    <!--<p class="" style="font-size: 12px;    color: black;">
                        Is the 133rd ranked of 674 active Brazil Pro Welterweights.<br>Born: Brazil<br>Fighting out of: Manaus, Amazonas,Brazil<br>MMA record 6-2-0 (Win-Loss-Draw)<br><br>Fight History
                        <br>
                        <br><i class="fa fa-trophy" aria-hidden="true"></i> Event: STFC 42: Rodriguez vs. Bustillos • Fighter: Danny Salinas • May 19, 2017
                        <br><i class="fa fa-times" aria-hidden="true"></i> Event: Mr. Cage 23 • Fighter: Adall Jackson • Aug 27, 2015
                        <br><i class="fa fa-trophy" aria-hidden="true"></i> Event: Big Way Fight 8 • Fighter: Anderson Barros • Dec 10, 2015
                        <br><i class="fa fa-trophy" aria-hidden="true"></i> Event: Samurai Figt Championship 2 • Fighter: Marco Tavares • Nov 29, 2015
                        <br><i class="fa fa-trophy" aria-hidden="true"></i> Event: Kratos Fight 2 • Fighter: Adriano Taveira • Nov 15, 2014
                        <br><i class="fa fa-times" aria-hidden="true"></i> Event: Rel da Selva Combat 3 • Fighter: Helderson Leal M. • Jun 08, 2014
                        <br><i class="fa fa-trophy" aria-hidden="true"></i> Event: It’s Time Combat 3 • Fighter: Rallson Silva Brandao • Jul 13, 2013
                        <br><i class="fa fa-trophy" aria-hidden="true"></i> Event: Coroado Fight Night • Fighter: Sergio Barboza • Mar 03, 2012
                      </p>-->
                     <p class="mb-0 text-center" style="font-size: 12px;color: black;">Born: Brazil<br>Fighting out of: Manaus, Amazonas,Brazil<br>MMA record 7-2-0 (Win-Loss-Draw)<br></p>
                  </div>
                      </div>
                            </div>
                            
                            <div class="col-md-6 col-sm-6 mb-sm-50 mb-xs-30 text-center">
                                <img class="mt-140 mt-xs-40" alt="" src="<?php echo base_url('public/theme') ?>/img/img_about.png">
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </section>
            <!-- End About Section -->
            
       
            <!--
<section>
        <div class="container">
            <div class="row v-align-children">
                <div class="col-sm-5 mb-xs-24 col-md-6">
                  <h1 class="text-center">ARTE SUAVE ACADEMY</h1>
                  <hr style=" top: -19px;    position: relative;   border-top: 7px solid red;    width: 24%;margin-left: 33%;">
                    <h3 class="bold">Instructor Edmilson Freitas "Junior"</h3>
                    <p class="hidden-xs" style="font-size: 12px;    color: black;">
                        Is the 133rd ranked of 674 active Brazil Pro Welterweights.<br>Born: Brazil<br>Fighting out of: Manaus, Amazonas,Brazil<br>MMA record 6-2-0 (Win-Loss-Draw)<br><br>Fight History
                        <br>
                        <br><i class="fa fa-trophy" aria-hidden="true"></i> Event: STFC 42: Rodriguez vs. Bustillos • Fighter: Danny Salinas • May 19, 2017
                        <br><i class="fa fa-times" aria-hidden="true"></i> Event: Mr. Cage 23 • Fighter: Adall Jackson • Aug 27, 2015
                        <br><i class="fa fa-trophy" aria-hidden="true"></i> Event: Big Way Fight 8 • Fighter: Anderson Barros • Dec 10, 2015
                        <br><i class="fa fa-trophy" aria-hidden="true"></i> Event: Samurai Figt Championship 2 • Fighter: Marco Tavares • Nov 29, 2015
                        <br><i class="fa fa-trophy" aria-hidden="true"></i> Event: Kratos Fight 2 • Fighter: Adriano Taveira • Nov 15, 2014
                        <br><i class="fa fa-times" aria-hidden="true"></i> Event: Rel da Selva Combat 3 • Fighter: Helderson Leal M. • Jun 08, 2014
                        <br><i class="fa fa-trophy" aria-hidden="true"></i> Event: It’s Time Combat 3 • Fighter: Rallson Silva Brandao • Jul 13, 2013
                        <br><i class="fa fa-trophy" aria-hidden="true"></i> Event: Coroado Fight Night • Fighter: Sergio Barboza • Mar 03, 2012
                      </p>
                      <p class="visible-xs" style="font-size: 12px;    color: black;">Is the 133rd ranked of 674 active Brazil Pro Welterweights.<br>Born: Brazil<br>Fighting out of: Manaus, Amazonas,Brazil<br>MMA record 6-2-0 (Win-Loss-Draw)<br><br>Fight History</p>
                      <p class="visible-xs" style="font-size: 12px;    color: black;"><i class="fa fa-trophy" aria-hidden="true"></i> Event: STFC 42: Rodriguez vs. Bustillos • Fighter: Danny Salinas • May 19, 2017</p>
                      <p style="font-size: 12px;    color: black;"><i class="fa fa-times" aria-hidden="true"></i> Event: Mr. Cage 23 • Fighter: Adall Jackson • Aug 27, 2015</p>
                      <p class="visible-xs" style="font-size: 12px;    color: black;"><i class="fa fa-trophy" aria-hidden="true"></i> Event: Big Way Fight 8 • Fighter: Anderson Barros • Dec 10, 2015</p>
                      <p class="visible-xs" style="font-size: 12px;    color: black;"><i class="fa fa-trophy" aria-hidden="true"></i> Event: Samurai Figt Championship 2 • Fighter: Marco Tavares • Nov 29, 2015</p>
                      <p class="visible-xs" style="font-size: 12px;    color: black;"><i class="fa fa-trophy" aria-hidden="true"></i> Event: Kratos Fight 2 • Fighter: Adriano Taveira • Nov 15, 2014</p>
                      <p class="visible-xs" style="font-size: 12px;    color: black;"><i class="fa fa-times" aria-hidden="true"></i> Event: Rel da Selva Combat 3 • Fighter: Helderson Leal M. • Jun 08, 2014</p>
                      <p class="visible-xs" style="font-size: 12px;    color: black;"><i class="fa fa-trophy" aria-hidden="true"></i> Event: It’s Time Combat 3 • Fighter: Rallson Silva Brandao • Jul 13, 2013</p>
                      <p class="visible-xs" style="font-size: 12px;    color: black;"><i class="fa fa-trophy" aria-hidden="true"></i> Event: Coroado Fight Night • Fighter: Sergio Barboza • Mar 03, 2012</p>
                </div>
                <div class="col-md-7 col-md-offset-1 col-sm-6 col-sm-offset-1 text-center">
                    <img style="    width: 520px;" class="cast-shadow" alt="Screenshot" src="<?php echo base_url('public/theme') ?>/img/img_about.png">
                </div>
            </div>

        </div>

    </section>
-->