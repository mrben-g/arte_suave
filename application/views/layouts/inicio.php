<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Arte Suave</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/_css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/_css/style.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/_css/style-responsive.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/_css/animate.min.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/_css/vertical.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/_css/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/_css/magnific-popup.css">  
        
        
	<link href="<?php echo base_url('public/theme') ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
	<link href="<?php echo base_url('public/theme') ?>/css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo base_url('public/theme') ?>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo base_url('public/theme') ?>/css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo base_url('public/theme') ?>/css/theme.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo base_url('public/theme') ?>/css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <style media="screen">
            
            html {font-family: 'avenirregular' !important;}
            .fa {font-family: 'FontAwesome' !important;}
            .footer_new {
                bottom: 0px;
                left: 0px;
                right: 0px;
                margin-bottom: 0px;
            }
            .col-sm-4 a{ color: #848484;}
            .font_4 a{ color: #848484;}
            .menu li a:hover {color: #ff0000;}
            .menu li a{/*font-weight: bold;*/}
            .row-top{background-color: black;}
            .row-top p {
                font-size: 10px;
                letter-spacing: 1px;
                color: white;
                margin-bottom: 2px;
            }
            .paralelogramo {
                width: 150px;
                height: 53px;
                background: #ff0000;
                position: absolute;
                -webkit-transform: skew(-20deg);
                -moz-transform: skew(-20deg);
                -ms-transform: skew(-20deg);
                -o-transform: skew(-20deg);
                transform: skew(-20deg);
                right: 12%;
            }
            .p_new{
                font-family: 'avenir_black_oblique';
                color: white;
                margin-bottom: 9px;
                margin-left: 34px;
                position: absolute;
                transform: skew(20deg);
            }
            .fa-circle{
                font-size: 8px;
            }
            h3{
                font-size: 21px !important;
                font-weight: bold;
            }
            h1{
                font-weight: bold;
                font-size: 31px;
                font-family: 'avenirblack' !important;
            }
            hr{color: #848484;}
            .footer_wh {background-color: white;}
           .bg-primary {background: #848484 !important;}
           .bg-primary .list-inline i {color: white;}
	</style>

</head>
<body class="appear-animate">
    <!-- Page Loader -->
    <div class="page-loader">
        <div class="loader">Loading...</div>            
    </div>
    <!-- End Page Loader -->
    
    <!-- Page Wrap -->
     <div class="page" id="top">
         
         
         <!-- Top Bar -->
            <div class="top-bar text-center hidden-xs">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2 text-right"></div>
                            <div class="col-md-8 text-center">
                                MMA (Mixed Martial Arts) <i class="fa fa-circle" aria-hidden="true"></i> Brazilian Jiu Jitsu <i class="fa fa-circle" aria-hidden="true"></i> Muay Thai <i class="fa fa-circle" aria-hidden="true"></i> Self Defence <i class="fa fa-circle" aria-hidden="true"></i> Kids Classes <i class="fa fa-circle" aria-hidden="true"></i> Fitness Program
                            </div>
                            <div class="col-md-2 text-right">
                                <div class="paralelogramo hidden-xs" style="z-index:9999;">
                <img style="position: absolute;top: 29%;right: 82%;transform: skew(20deg);" src="<?php echo base_url('public/theme/icons/header_phone_icon.png') ?>" alt="">
                <p class="p_new" style="    top: 4px;">GET IN TOUCH</p>
                <p style="    top: 42%;" class="p_new">(956) 342-9467</p>
            </div>
                                
                            </div>
                        </div>
                        </div>		  
                </div>            
            <!-- End Top Bar -->
          
            
	<!--<div class="row row-top text-center hidden-xs">
			<p>MMA (Mixed Martial Arts) <i class="fa fa-circle" aria-hidden="true"></i> Brazilian Jiu Jitsu <i class="fa fa-circle" aria-hidden="true"></i> Muay Thai <i class="fa fa-circle" aria-hidden="true"></i> Self Defence <i class="fa fa-circle" aria-hidden="true"></i> Kids Classes <i class="fa fa-circle" aria-hidden="true"></i> Fitness Program</p>		
	</div>-->
	<!-- Movil -->
	<div class="row visible-xs">

	</div>
	<!-- Movil -->
<!-- Navigation panel -->
<div class="clearfix visible-xs visible-sm">
            <nav class="main-nav js-stick ">
                <div class="container relative clearfix">
                    <!-- Logo ( * your text or image into link tag *) -->
                    <div class="nav-logo-wrap local-scroll">
                        <a href="<?php echo base_url() ?>" class="logo">
                                 <img  alt="logo" class="mt-xs-10 mt-sm-10" src="<?php echo base_url('public/Logo_artesuave.png') ?>">
                             </a>
                    </div>
                    <div class="mobile-nav">
                        <i class="fa fa-bars"></i>
                    </div>
                    <!-- Main Menu -->
                    <div class="inner-nav desktop-nav">
                        <ul class="clearlist scroll-nav local-scroll">
                                <li><a style="<?php echo (!$this->uri->segment(1))?'color:#ff0000;font-weight:bold;':'' ?>" href="<?php echo base_url() ?>">Home </a></li>
                            <li><a style="<?php echo ($this->uri->segment(1) == 'about')?'color:#ff0000;font-weight:bold;':'' ?>" href="<?php echo base_url('about') ?>">About</a></li>
                            <li><a style="<?php echo ($this->uri->segment(1) == 'gallery')?'color:#ff0000;font-weight:bold;':'' ?>" href="<?php echo base_url('gallery') ?>">Gallery </a></li>
                            <li><a style="<?php echo ($this->uri->segment(1) == 'our_programs')?'color:#ff0000;font-weight:bold;':'' ?>" href="<?php echo base_url('our_programs') ?>">Our Programs</a></li>
                            <li><a style="<?php echo ($this->uri->segment(1) == 'calendar')?'color:#ff0000;font-weight:bold;':'' ?>" href="<?php echo base_url('calendar') ?>">Schedule</a></li>
                            <li><a style="<?php echo ($this->uri->segment(1) == 'contact')?'color:#ff0000;font-weight:bold;':'' ?>" href="<?php echo base_url('contact') ?>">Contact</a></li>     
                            <!-- Button --
                            <li>
                                <a href="#"><span class="btn btn-mod btn-circle"><i class="fa fa-shopping-cart"></i> </span></a>
                            </li>
                            <!-- End Button -->
                            
                        </ul>
                    </div>
                </div>
            </nav>
</div>
            <!-- End Navigation panel -->
 
<!-- Nav desktop -->
<div class="container hidden-xs hidden-sm" style="">
    <nav class="main-nav js-stick">
         <div class="container relative clearfix">
        <div class="nav-bar text-center mt-0 mb-0" style="">
            <div class="col-md-1 col-sm-1 text-center" ></div>
            <div class="col-md-4 col-sm-4 text-center" style="line-height:114px;">
                <ul class="menu inline-block">
                    <li><a style="<?php echo (!$this->uri->segment(1))?'color:#ff0000;font-weight:bold;':'' ?>" href="<?php echo base_url() ?>">Home </a></li>
                    <li><a style="<?php echo ($this->uri->segment(1) == 'about')?'color:#ff0000;font-weight:bold;':'' ?>" href="<?php echo base_url('about') ?>">About</a></li>
                    <li><a style="<?php echo ($this->uri->segment(1) == 'gallery')?'color:#ff0000;font-weight:bold;':'' ?>" href="<?php echo base_url('gallery') ?>">Gallery </a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-2 text-center">
                <a href="#" class="logo">
                    <img  alt="logo"  src="<?php echo base_url('public/Logo_artesuave.png') ?>" style="" class="mt-10">
                </a>
            </div>
            <div class="col-md-5 col-sm-5 text-center" style="line-height:114px;">
                <ul class="menu inline-block">
                    <li><a style="<?php echo ($this->uri->segment(1) == 'our_programs')?'color:#ff0000;font-weight:bold;':'' ?>" href="<?php echo base_url('our_programs') ?>">Our Programs</a></li>
                    <li><a style="<?php echo ($this->uri->segment(1) == 'calendar')?'color:#ff0000;font-weight:bold;':'' ?>" href="<?php echo base_url('calendar') ?>">Schedule</a></li>
                    <li><a style="<?php echo ($this->uri->segment(1) == 'contact')?'color:#ff0000;font-weight:bold;':'' ?>" href="<?php echo base_url('contact') ?>">Contact</a></li>
                </ul>
            </div>
        </div>
        
        <div class="module widget-handle mobile-toggle right visible-sm visible-xs absolute-xs">
            <i class="ti-menu"></i>
        </div>
         </div>
    </nav>
</div>
	<!-- Nav Desktop -->

	<!-- Nav Mobile --
	<nav class="navbar navbar-default visible-xs">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display --
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">
				<img width="30px" src="<?php echo base_url('public/Logo_artesuave.png') ?>" alt="">
			</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling --
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> --
        <li class="<?php echo (!$this->uri->segment(1) )? 'active' : '' ; ?>"><a href="<?php echo base_url(''); ?>">Home</a></li>
				<li class="<?php echo ($this->uri->segment(1) == 'about' )? 'active' : '' ; ?>"><a href="<?php echo base_url('about'); ?>">About</a></li>
				<li class="<?php echo ($this->uri->segment(1) == 'gallery' )? 'active' : '' ; ?>"><a href="<?php echo base_url('gallery'); ?>">Gallery</a></li>
				<li class="<?php echo ($this->uri->segment(1) == 'our_programs' )? 'active' : '' ; ?>"><a href="<?php echo base_url('our_programs'); ?>">Our programs</a></li>
				<li class="<?php echo ($this->uri->segment(1) == 'calendar' )? 'active' : '' ; ?>"><a href="<?php echo base_url('calendar'); ?>">Schedule</a></li>
				<li class="<?php echo ($this->uri->segment(1) == 'contact' )? 'active' : '' ; ?>"><a href="<?php echo base_url('contact'); ?>">Contact</a></li>
      </ul>


    </div><!-- /.navbar-collapse --
  </div><!-- /.container-fluid --
</nav>
	<!-- Nav mobile -->

	<div class="main-container">
		<?php echo $this->load->view($this->data['vista'], array(),true); ?>
	</div>
        
        
        

	<!-- Footer Mobile --
	<footer class="footer-2 bg-primary visible-xs">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center mb64 mb-xs-24">
					<a href="#">
						<img alt="Logo" class="image-xs mb16" src="<?php echo base_url('public/Logo_artesuave.png') ?>">
					</a>
					<p class="lead mb48 mb-xs-16">
						2407 Brock • St. Unit 20 Mission, TX<br> <br>
						(956) 342 9467
					</p>
					<ul class="list-inline social-list spread-children">
						<li><a href="#"><i class="fa fa-phone-square"></i></a></li>
						<li><a href="#"><i class="fa fa-map-marker"></i></a></li>
						<li><a href="#"><i class="fa fa-envelope"></i></a></li>
						<!-- <li><a href="#"><i class="icon icon-sm ti-vimeo-alt"></i></a></li> --
					</ul>
				</div>
			</div>

			<div class="row fade-half">
				<div class="col-sm-4 text-center-xs">
					<span>© Copyright <?php echo date('Y') ?> Mr.Wasabi</span>
				</div>

				<div class="col-sm-4 text-center hidden-xs">
					<span>Contact us for a free trial class!</span>
				</div>

				<div class="col-sm-4 text-right hidden-xs">
					<span>artesuavebjjmission@gmail.com</span>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer Mobile -->

	<!--<div style="margin-bottom:0%;" class="row footer_new hidden-xs">
	  <div style="margin-top:2%;" class="">

			<hr style="    top: 12px;
    position: relative;    border-top: 1px solid #848484;">

	  </div>
	    <div>
	    <div class="col-sm-6 font_4 footer_wh">
	      <a style="margin-left:18%; margin-right:5%;" href="#">Arte Suave 2017</a>
	      <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> 2407 Brock St. Unit 20 Mission TX.</a>
	    </div>


	    <div style="text-align:right;" class="col-sm-6 font_4 footer_wh">
	      <a style="margin-right:5%;" href="#"> <img src="<?php echo base_url('public/theme/icons/phone_icon.png') ?>" alt=""> (956) 342-9467</a>
	      <a style="margin-right:1%;" href="#"><img src="<?php echo base_url('public/theme/icons/email_icon.png') ?>" alt=""> artesuavejjmission@gmail.com</a>
				<a style="margin-right:18%;" href="#"><img src="<?php echo base_url('public/theme/icons/fb__icon.png') ?>" alt=""></a>
	    </div>
	  </div>

	</div>-->
         
          <!-- Foter -->
            <footer class="page-section footer pb-10 pt-10">
                <!-- Divider -->
            <hr class="mt-20 mb-10 pb-10"/>
            <!-- End Divider -->
                <div class="container">
                    <div class="col-sm-6 text-left">
                        <a  href="#" style="margin-right:5px">Arte Suave 2017 &copy;</a>
                        <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> 228 N.Ware Rd. Suite 30 McAllen Tx. 78501</a>
                        
<!--                         2407 Brock St. Unit 20 Mission TX.-->
	    </div>
                    <div class="col-sm-6 text-right">
	      <a style="" href="#" style="margin-right:5px">
                  <span class="mn-soc-link" title="" style="width: 20px;height: 20px;line-height: 20px;"><i class="fa fa-phone"></i></span> (956) 342-9467</a>     
	      <a style="" href="#" style="margin-right:5px">
                  <span class="mn-soc-link" title="" style="width: 20px;height: 20px;line-height: 20px;"><i class="fa fa-envelope-o"></i></span> artesuavejjmission@gmail.com</a>     
	     <a style="" href="#">
                 <span class="mn-soc-link" title="" style="width: 20px;height: 20px;line-height: 20px;"><i class="fa fa-facebook"></i></span></a>
	    </div>
                    
                    <!-- Social Links --
                    <div class="footer-social-links mb-110 mb-xs-60">
                        <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="#" title="Behance" target="_blank"><i class="fa fa-behance"></i></a>
                        <a href="#" title="LinkedIn+" target="_blank"><i class="fa fa-linkedin"></i></a>
                        <a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest"></i></a>
                    </div>
                    <!-- End Social Links -->  
                    
                    <!-- Footer Text --
                    <div class="footer-text">
                        
                        <!-- Copyright --
                        <div class="footer-copy font-alt">
                            <a href="" target="_blank"></a>.
                        </div>
                        <!-- End Copyright --
                        
                        <div class="footer-made">
                            
                        </div>
                        
                    </div>
                    <!-- End Footer Text --> 
                    
                 </div>
                 
                 <?php  if($this->uri->segment(1) == 'our_programs'){  ?>
                 <!-- Top Link -->
                 <div class="local-scroll">
                     <a href="#top" class="link-to-top"><i class="fa fa-caret-up"></i></a>
                 </div>
                 <?php } ?>
                 <!-- End Top Link -->
                 
            </footer>
            <!-- End Foter -->
     </div>
<!--	<script src="<?php echo base_url('public/theme') ?>/js/jquery.min.js"></script>
	<script src="<?php echo base_url('public/theme') ?>/js/bootstrap.min.js"></script>-->
	
        
        
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.mobile.customized.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/bootstrap.min.js"></script>        
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/SmoothScroll.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.localScroll.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.viewport.mini.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.countTo.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.appear.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.sticky.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.fitvids.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.magnific-popup.min.js"></script>
<!--<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/jquery.bxslider.min.js"></script>-->
<!-- Replace test API Key "AIzaSyAZsDkJFLS0b59q7cmW0EprwfcfUA8d9dg" with your own one below 
**** You can get API Key here - https://developers.google.com/maps/documentation/javascript/get-api-key -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJttQY3k3BZzwVNmaRgyOSe6Bkvgnp4o0"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/gmap3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/wow.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.simple-text-rotator.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/all.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/contact-form.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.ajaxchimp.min.js"></script>                
        <script src="<?php echo base_url('public/theme') ?>/js/flexslider.min.js"></script>
	<script src="<?php echo base_url('public/theme') ?>/js/parallax.js"></script>
	<script src="<?php echo base_url('public/theme') ?>/js/scripts.js"></script>
        
  
        
</body>

</html>
